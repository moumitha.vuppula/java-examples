package day3;

@FunctionalInterface
public interface PaymentGateway {
    void pay(String from, String to, double amount, String notes) throws PaymentGatewayException;
}

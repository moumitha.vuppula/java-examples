package day3;

import java.util.*;

public class MapDemo {

    public static void main(String[] args) {
        Map<Integer, String> mapRef = new HashMap<>();
        mapRef.put(12, "twelve");
        mapRef.put(13, "thirteen");
        mapRef.put(14, "fourteen");
        mapRef.put(15, "fifteen");
        mapRef.put(12, "TWELVE");

/*      System.out.println("Value with the key 12 is " +mapRef.get(12));
        System.err.println("Size  of the map is " +mapRef.size());
        System.out.println("Map contains 12 "+mapRef.containsKey(12));
        System.out.println("Map contains thirteen "+mapRef.containsValue("thirteenNotPresent"));
        System.out.println(mapRef.getOrDefault(18, "SOME Random default value"));
 */
       Set<Integer> keys = mapRef.keySet();     
       
       //1. Iterator
       Iterator<Integer> it = keys.iterator();
       while(it.hasNext()){
          Integer key = it.next();
          System.out.println("Key "+ key);
          System.out.println("Value "+ mapRef.get(key));
       }

       //printing the values
       Collection<String> values = mapRef.values();

        Set<Entry<Integer,String>> entrySet = mapRef.entrySet();

        Iterator<Entry<Integer,String>> iterator = entrySet.iterator();
        while(iterator.hasNext()){
            Entry<Integer, String> entry = iterator.next();
            System.out.print("Key "+ entry.getKey() + "\t");
            System.out.println("Value "+ entry.getValue());
        }
    }
}

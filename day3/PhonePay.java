package day3;

public class PhonePay implements PaymentGateway, PhoneRecharge{

    @Override
    public void pay(String from, String to, double amount, String notes) {
        System.out.printf("Paying Rs %s from %s to %s using Phone Pay %n", amount, from, to);        
        System.out.println(" You have a cashback of Rs "+ 0.05 * amount);
    }

    @Override
    public void recharge(String phoneNumber, double amount) {
        System.out.printf("Successfully recharged Phone number: %s with  Rs %s  using Phone Pay %n", phoneNumber, amount);        
        System.out.println(" You have earned a reward point of Rs: "+ 0.01 * amount);
    }
}

package day2;

import java.util.ArrayList;
import java.util.List;

public class PermanentEmployee extends Employee {

    private double salary;

    private int leaves = 20;

    private List<Dependent> dependents = new ArrayList<>();

    public PermanentEmployee(String name, String emailAddress){
        super(name, emailAddress);
    }
    public PermanentEmployee(String name, String emailAddress, double salary){
        super(name, emailAddress);
        this.salary = salary;
    }
    
    public void setSalary(double salary){
        this.salary = salary;
    }

    public double getSalary(){
        return this.salary;
    }

    public void addDependent(Dependent dependent){
        this.dependents.add(dependent);
    }

    public List<Dependent> getDependents(){
        return this.dependents;
    }

    @Override
    public void applyForLeave(int noOfDays) throws InsufficientLeaveBalanceException{
        if(this.leaves > noOfDays){
            this.leaves = this.leaves - noOfDays;
        }else {
            throw new InsufficientLeaveBalanceException("Insufficient leave balance.");
        }
    }

    @Override
    public double calculateTDS(){
        return (this.salary * 10/100);
    }

    public int getLeaves(){
        return this.leaves;
    }

}

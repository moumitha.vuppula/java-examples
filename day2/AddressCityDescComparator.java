package day2;

import java.util.Comparator;

public class AddressCityDescComparator implements Comparator<Address>{

    public int compare(Address address1, Address address2){
        return address2.getCity().compareTo(address1.getCity());
    }
    
}
